<aside id="sidebar_left" class="nano nano-light affix">

        <!-- -------------- Sidebar Left Wrapper  -------------- -->
        <div class="sidebar-left-content nano-content">

            <!-- -------------- Sidebar Header -------------- -->
            <header class="sidebar-header">

                <!-- -------------- Sidebar - Author -------------- -->
                <div class="sidebar-widget author-widget">
                    <div class="media">
                        <a class="media-left" href="#">
                            <img src="assets/img/avatars/profile_avatar.jpg" class="img-responsive">
                        </a>

                        <div class="media-body">
                            <div class="media-links">
                                <a href="#" class="sidebar-menu-toggle">User Menu -</a> <a href="logout.php">Logout</a>
                            </div>
                            <div class="media-author">Douglas Adams</div>
                        </div>
                    </div>
                </div>

                <!-- -------------- Sidebar - Author Menu  -------------- -->
                <div class="sidebar-widget menu-widget">
                    <div class="row text-center mbn">
                        <div class="col-xs-2 pln prn">
                            <a href="dashboard1.html" class="text-primary" data-toggle="tooltip" data-placement="top"
                               title="Dashboard">
                                <span class="fa fa-dashboard"></span>
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-2 pln prn">
                            <a href="charts-highcharts.html" class="text-info" data-toggle="tooltip"
                               data-placement="top"
                               title="Stats">
                                <span class="fa fa-bar-chart-o"></span>
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-2 pln prn">
                            <a href="sales-stats-products.html" class="text-system" data-toggle="tooltip"
                               data-placement="top" title="Orders">
                                <span class="fa fa-info-circle"></span>
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-2 pln prn">
                            <a href="sales-stats-purchases.html" class="text-warning" data-toggle="tooltip"
                               data-placement="top" title="Invoices">
                                <span class="fa fa-file"></span>
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-2 pln prn">
                            <a href="basic-profile.html" class="text-alert" data-toggle="tooltip" data-placement="top"
                               title="Users">
                                <span class="fa fa-users"></span>
                            </a>
                        </div>
                        <div class="col-xs-4 col-sm-2 pln prn">
                            <a href="management-tools-dock.html" class="text-danger" data-toggle="tooltip"
                               data-placement="top" title="Settings">
                                <span class="fa fa-cogs"></span>
                            </a>
                        </div>
                    </div>
                </div>

            </header>
            <!-- -------------- /Sidebar Header -------------- -->

            <!-- -------------- Sidebar Menu  -------------- -->
            <ul class="nav sidebar-menu">
                <li class="sidebar-label pt30">Menu</li>

                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-dashboard"></span>
                        <span class="sidebar-title">Agents</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="#">
                                <span class="fa fa-file-text-o"></span> Add Agent </a>
                        </li>
                        <li >
                            <a href="#">
                                <span class="fa fa-file-text-o"></span> View Agents </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-dashboard"></span>
                        <span class="sidebar-title">Clients</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="#">
                                <span class="fa fa-file-text-o"></span> Add Client </a>
                        </li>
                        <li >
                            <a href="#">
                                <span class="fa fa-file-text-o"></span> View Clients </a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-label pt25">Tools</li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-share-square-o"></span>
                        <span class="sidebar-title">Sales stats</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="dashboard2.html">
                                <span class="glyphicon glyphicon-tags"></span> Overview </a>
                        </li>
                        <li>
                            <a href="sales-stats-products.html">
                                <span class="glyphicon glyphicon-tags"></span> Products </a>
                        </li>
                        <li>
                            <a href="sales-stats-purchases.html">
                                <span class="fa fa-money"></span> Purchases </a>
                        </li>
                        <li>
                            <a href="sales-stats-clients.html">
                                <span class="fa fa-users"></span> Clients </a>
                        </li>
                        <li>
                            <a href="sales-stats-general-settings.html">
                                <span class="fa fa-gears"></span> General Settings </a>
                        </li>
                    </ul>
                </li>


                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-desktop"></span>
                        <span class="sidebar-title">Layout Templates</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa fa-arrows-h"></span>
                                Sidebars
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="sidebar-left-static.html">
                                        Left Static </a>
                                </li>
                                <li>
                                    <a href="sidebar-left-fixed.html">
                                        Left Fixed </a>
                                </li>
                                <li>
                                    <a href="sidebar-left-minified.html">
                                        Left Minified </a>
                                </li>
                                <li>
                                    <a href="sidebar-right-fixed.html">
                                        Right Fixed </a>
                                </li>
                                <li>
                                    <a href="sidebar-right-menu.html">
                                        Right Static </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-arrows-v"></span>
                                Navigation
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="navigation-static.html">
                                        Static </a>
                                </li>
                                <li>
                                    <a href="navigation-fixed.html">
                                        Fixed </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-hand-o-up"></span>
                                Top Panel
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="top-panel.html">
                                        Default </a>
                                </li>
                                <li>
                                    <a href="top-panel-menu.html">
                                        With Menu </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-arrows-v"></span>
                                Content
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="content-blank.html">
                                        Blank </a>
                                </li>
                                <li>
                                    <a href="content-fixed.html">
                                        Fixed </a>
                                </li>
                                <li>
                                    <a href="content-hero.html">
                                        Hero Content </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-pause"></span>
                                Content Chutes
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="chute-left.html">
                                        Left Static </a>
                                </li>
                                <li>
                                    <a href="chute-left-fixed.html">
                                        Left Fixed </a>
                                </li>
                                <li>
                                    <a href="chute-right.html">
                                        Right Static </a>
                                </li>
                                <li>
                                    <a href="chute-right-fixed.html">
                                        Right Fixed </a>
                                </li>
                                <li>
                                    <a href="chute-both.html">
                                        Left &amp; Right Static </a>
                                </li>
                                <li>
                                    <a href="chute-both-fixed.html">
                                        Left &amp; Right Fixed </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-plus-square-o"></span>
                                Boxed Frontpage
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="boxed-default.html">
                                        Default </a>
                                </li>
                                <li>
                                    <a href="horizontal-navigation-boxed.html">
                                        Optional Navigation </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-arrow-circle-o-up"></span>
                                Horizontal Navigation
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="horizontal-navigation-small-menu.html">
                                        Small Menu</a>
                                </li>
                                <li>
                                    <a href="horizontal-navigation-medium-menu.html">
                                        Medium Menu</a>
                                </li>
                                <li>
                                    <a href="horizontal-navigation-large-menu.html">
                                        Large Menu</a>
                                </li>
                                <li>
                                    <a href="horizontal-navigation-top-panel.html">
                                        With Top Panel</a>
                                </li>
                                <li>
                                    <a href="horizontal-navigation-collapsing-top-panel.html">
                                        Collapsing Top Panel</a>
                                </li>
                                <li>
                                    <a href="horizontal-navigation-boxed.html">
                                        Boxed Layout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-wrench"></span>
                        <span class="sidebar-title">Management Tools</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="management-tools-panels.html">
                                <span class="glyphicon glyphicon-book"></span> Panels </a>
                        </li>
                        <li>
                            <a href="management-tools-modals.html">
                                <span class="glyphicon glyphicon-modal-window"></span> Modals </a>
                        </li>
                        <li>
                            <a href="management-tools-dock.html">
                                <span class="glyphicon glyphicon-equalizer"></span> Dock </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-check-square-o"></span>
                        <span class="sidebar-title">Forms</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="forms-elements.html">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Elements </a>
                        </li>
                        <li>
                            <a href="forms-widgets.html">
                                <span class="glyphicon glyphicon-calendar"></span> Widgets </a>
                        </li>
                        <li>
                            <a href="forms-layouts.html">
                                <span class="fa fa-desktop"></span> Layouts </a>
                        </li>
                        <li>
                            <a href="forms-wizard.html">
                                <span class="fa fa-clipboard"></span> Wizard </a>
                        </li>
                        <li>
                            <a href="forms-validation.html">
                                <span class="glyphicon glyphicon-check"></span> Validation </a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-label pt30">Elements</li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-cogs"></span>
                        <span class="sidebar-title">Widgets</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="widgets-panels.html">
                                <span class="fa fa-desktop"></span> Panels </a>
                        </li>
                        <li>
                            <a href="widgets-scrollers-tiles.html">
                                <span class="fa fa-columns"></span> Scrollers &amp; Tiles</a>
                        </li>
                        <li>
                            <a href="widgets-tools.html">
                                <span class="fa fa-dot-circle-o"></span> Tools </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="email-layouts.html">
                        <span class="fa fa-envelope-o"></span>
                        <span class="sidebar-title">Email Layouts</span>
                    </a>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-star-half-full "></span>
                        <span class="sidebar-title">User Interface</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="user-interface-alerts.html">
                                <span class="fa fa-warning"></span> Alerts </a>
                        </li>
                        <li>
                            <a href="user-interface-buttons.html">
                                <span class="fa fa-plus-square-o"></span> Buttons </a>
                        </li>
                        <li>
                            <a href="user-interface-typography.html">
                                <span class="fa fa-text-width"></span> Typography </a>
                        </li>
                        <li>
                            <a href="user-interface-panels.html">
                                <span class="fa fa-archive"></span> Panels </a>
                        </li>
                        <li>
                            <a href="user-interface-progress-bars.html">
                                <span class="fa fa-bars"></span> Progress Bars </a>
                        </li>
                        <li>
                            <a href="user-interface-tabs.html">
                                <span class="fa fa-toggle-off"></span> Tabs </a>
                        </li>
                        <li>
                            <a href="user-interface-icons.html">
                                <span class="fa fa-hand-o-right"></span> Icons </a>
                        </li>
                        <li>
                            <a href="user-interface-grid.html">
                                <span class="fa fa-th-large"></span> Grid </a>
                        </li>
                        <li>
                            <a href="user-interface-progress-loader.html">
                                <span class="fa fa-th-large"></span> Page Progress Loader </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-tasks"></span>
                        <span class="sidebar-title">User Forms</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a href="user-forms-standart-inputs.html">
                                <span class="fa fa-magic"></span> Standart Inputs </a>
                        </li>
                        <li>
                            <a href="user-forms-additional-inputs.html">
                                <span class="fa fa-bell-o"></span> Additional Inputs
                            </a>
                        </li>
                        <li>
                            <a href="user-forms-editors.html">
                                <span class="fa fa-clipboard"></span> Editors </a>
                        </li>
                        <li>
                            <a href="user-forms-treeview.html">
                                <span class="fa fa-tree"></span> Treeview </a>
                        </li>
                        <li>
                            <a href="user-forms-nestable.html">
                                <span class="fa fa-tasks"></span> Nestable </a>
                        </li>
                        <li>
                            <a href="user-forms-image-tools.html">
                                <span class="fa fa-cloud-upload"></span> Image Tools
                            </a>
                        </li>
                        <li>
                            <a href="user-forms-file-uploaders.html">
                                <span class="fa fa-cloud-upload"></span> File Uploaders </a>
                        </li>
                        <li>
                            <a href="user-forms-notifications.html">
                                <span class="fa fa-bell-o"></span> Notifications </a>
                        </li>
                        <li>
                            <a href="user-forms-content-sliders.html">
                                <span class="fa fa-exchange"></span> Content Sliders </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-crop"></span>
                        <span class="sidebar-title">Plugins</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="glyphicon glyphicon-globe"></span> Maps
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="maps-basic.html">Basic</a>
                                </li>
                                <li>
                                    <a href="maps-vector.html">Vector</a>
                                </li>
                                <li>
                                    <a href="maps-full.html">Full</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-area-chart"></span> Charts
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="charts-highcharts.html">Highcharts</a>
                                </li>
                                <li>
                                    <a href="charts-d3.html">D3 Charts</a>
                                </li>
                                <li>
                                    <a href="charts-flot.html">Flot Charts</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-table"></span> Tables
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="tables-basic.html"> Basic </a>
                                </li>
                                <li>
                                    <a href="tables-datatables.html"> Data </a>
                                </li>
                                <li>
                                    <a href="tables-sortable.html"> Sortable </a>
                                </li>
                                <li>
                                    <a href="tables-pricing.html"> Pricing </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="accordion-toggle" href="#">
                        <span class="fa fa-file-text-o"></span>
                        <span class="sidebar-title">Pages</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="nav sub-nav">
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-gears"></span> Utility
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="utility-confirmation.html" target="_blank"> Confirmation </a>
                                </li>
                                <li>
                                    <a href="utility-login.html" target="_blank"> Login </a>
                                </li>
                                <li>
                                    <a href="utility-register.html" target="_blank"> Register </a>
                                </li>
                                <li>
                                    <a href="utility-forgot-password.html" target="_blank"> Forgot Password </a>
                                </li>
                                <li>
                                    <a href="utility-coming-soon.html" target="_blank"> Coming Soon
                                    </a>
                                </li>
                                <li>
                                    <a href="utility-404-error.html"> 404 Error </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a class="accordion-toggle" href="#">
                                <span class="fa fa-desktop"></span> Basic
                                <span class="caret"></span>
                            </a>
                            <ul class="nav sub-nav">
                                <li>
                                    <a href="basic-search-results.html">Search Results</a>
                                </li>
                                <li>
                                    <a href="basic-profile.html"> Profile </a>
                                </li>
                                <li>
                                    <a href="basic-timeline.html"> Timeline </a>
                                </li>
                                <li>
                                    <a href="basic-faq-page.html"> FAQ Page </a>
                                </li>
                                <li>
                                    <a href="basic-calendar.html"> Calendar </a>
                                </li>
                                <li>
                                    <a href="basic-messages.html"> Messages </a>
                                </li>
                                <li>
                                    <a href="basic-gallery.html"> Gallery </a>
                                </li>
                                <li>
                                    <a href="basic-invoice.html"> Invoice </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="doc/index.html">
                        <span class="fa fa-book"></span>
                        <span class="sidebar-title">Documentation</span>
                    </a>
                </li>

                <!-- -------------- Sidebar Progress Bars -------------- -->
                <li class="sidebar-label pt25 pb15">Stats</li>
                <li class="sidebar-stat">
                    <a href="#" class="fs11">
                        <span class="fa fa-calendar-o text-info"></span>
                        <span class="sidebar-title text-muted">September earnings</span>
                        <span class="pull-right mr20 text-muted">$1158</span>

                        <div id="high-column4" style="height: 150px;"></div>
                    </a>
                </li>
                <li class="sidebar-stat">
                    <a href="#" class="fs11">
                        <span class="fa fa-calendar text-info"></span>
                        <span class="sidebar-title text-muted">August earnings</span>
                        <span class="pull-right mr20 text-muted">$1001</span>

                        <div id="high-column5" style="height: 150px;"></div>
                    </a>
                </li>
            </ul>
            <!-- -------------- /Sidebar Menu  -------------- -->

            <!-- -------------- Sidebar Hide Button -------------- -->
            <div class="sidebar-toggler">
                <a href="#">
                    <span class="fa fa-arrow-circle-o-left"></span>
                </a>
            </div>
            <!-- -------------- /Sidebar Hide Button -------------- -->

        </div>
        <!-- -------------- /Sidebar Left Wrapper  -------------- -->

    </aside>